import asyncio

import discord
import requests

import glob
import os

import json

COMMAND_INITIAL_CHAR = "/"
intents = discord.Intents.default()
intents.message_content = True



def user_command(user_message, command_message, command_char=COMMAND_INITIAL_CHAR):
    return user_message.startswith(command_char + command_message)

def get_parameters(message):
    message_split = message.split(" ")
    if len(message_split) <= 1:
        # no parameters? return an empty list of params
        return []

    # return without the command
    return message_split[1:]


def parameters_check(parameters_need_amount, parameters, channel):

    parameters_amount = len(parameters)
    if parameters_need_amount < parameters_amount:
        send_message_in_channel(f"Not enough parameters, {parameters_need_amount} required", channel)
        return False

    if parameters_need_amount > parameters_amount:
        send_message_in_channel(f"Too manys parameters, {parameters_need_amount} required", channel)
        return False

    if parameters_need_amount == parameters_amount:
        return True

def send_message_in_channel(message, channel):
    asyncio.get_event_loop().create_task(channel.send(message))

# wipes always by host, sends message at the last message user's discord server
class FfClipMat(discord.Client):

    def __init__(self):
        super().__init__(intents=intents)
        self.database = DataBase()

        self.is_enabled = False

        self.TIME_BETWEEN_CHECKS = 1

        self.WAIT_TIME_FOR_PREVIEWS = 5

        self.wipes_amount = 0
        self.wipes_number = 0
        self.WIPE_CODE = "4000000F"
        self.before_run_wipes_amount = self.get_current_wipes_amount()
        self.CLIPS_URL = "https://clips.twitch.tv/"

        self.TOKEN = "MTEyNTcxNzM5NTE4NTYxNDg5MA.GepIN0.aLF0w5iA4aDzHlp8s9YLhvDSxsk3tSA2jA4wfI"

        self.clip_channel_id = None
        self.current_server_id = 0

        # {server_id: [twitch_clippers]}
        self.twitch_clippers = {}

    async def on_ready(self):
        print('We have logged in as {0.user}'.format(self))
        asyncio.create_task(self.look_for_any_wipes())
        print("ready now!!!!")

        for guild in self.guilds:
            # give each server a list of twitch clippers
            self.twitch_clippers[str(guild.id)] = []
            # if server not already in database, add it to that
            if str(guild.id) not in self.database.data:
                self.database.on_server_join(str(guild.id))
            for channel_name in self.database.get_channels(str(guild.id)):
                self.twitch_clippers[str(guild.id)].append(TwitchClipper(channel_name))




    def get_current_wipes_amount(self):
        list_of_files = glob.glob(
            'C:\\Users\\Matan.DESKTOP-AJJ3SA6\\AppData\\Roaming\\Advanced Combat Tracker\\FFXIVLogs\\*.log')  # * means all if need specific format then *.csv
        latest_file = max(list_of_files, key=os.path.getmtime)

        current_wipes_amount = 0

        with open(latest_file, 'r', encoding='utf8') as f:

            for line in f:
                if self.WIPE_CODE in line:
                    current_wipes_amount += 1

        return current_wipes_amount

    async def look_for_any_wipes(self):
        while True:
            await asyncio.sleep(self.TIME_BETWEEN_CHECKS)
            current_wipes_amount = self.get_current_wipes_amount()

            if current_wipes_amount > self.wipes_amount + self.before_run_wipes_amount:
                self.wipes_amount = current_wipes_amount - self.before_run_wipes_amount
                self.wipes_number += 1
                if self.is_enabled:

                    await self.handle_wipe()


    async def handle_wipe(self):
        for twitch_clipper in self.twitch_clippers[str(self.current_server_id)]:
            try:
                clip_id = twitch_clipper.clip()
                # wait a bit so when sending the clip, preview appears
                await asyncio.sleep(self.WAIT_TIME_FOR_PREVIEWS)
                await self.get_channel(self.clip_channel_id).send(f"Pull #{self.wipes_number} {self.CLIPS_URL + clip_id}")

            except Exception as e:
                await self.get_channel(self.clip_channel_id).send((str(e)))
                return

    def on_guild_join(self, guild):
        self.database.on_server_join(str(guild.id))

    async def on_message(self, message):
        if message.author == self.user:
            return

        self.current_server_id = int(message.guild.id)
        user_message = message.content

        # hello command
        if user_command(user_message, "hello"):
            await message.channel.send('Hello!')

        if user_command(user_message, "romper"):
            await message.channel.send("romepes")

        if user_command(user_message, "set_clip_channel"):
            self.set_clip_channel_command(message)

        if user_command(user_message, "test_clip"):
            self.test_clip_command(message)

        if user_command(user_message, "add_channel"):
            self.add_channel_command(message)

        if user_command(user_message, "start"):
            self.start_command(message)

        if user_command(user_message, "stop"):
            self.stop_command(message)

        if user_command(user_message, "remove_channel"):
            self.remove_channel_command(message)

        if user_command(user_message, "channels_list"):
            self.channels_list_command(message)

        if user_command(user_message, "help"):
            self.help_command(message)

    def help_command(self, message):
        send_message_in_channel("""Current commands:
        
        /set_clip_channel channel_id - the channel the clips would be sent in
        /add_channel channel_name - add a twitch channel to clip
        /remove_channel channel_name - remove an existing twitch channel
        /channels_list - lists all channels added
        /start - makes the bot post clips on wipes
        /stop - stops bot from posting any clips
        /help - posts this help message""", message.channel)
    def channels_list_command(self, message):

        channels = "Current channels added are: "

        channels += ", ".join(self.database.get_channels(str(self.current_server_id)))

        send_message_in_channel(channels, message.channel)

    def remove_channel_command(self, message):
        user_message = message.content

        parameters = get_parameters(user_message)

        if parameters_check(1, parameters, message.channel):
            try:

                channel_name = parameters[0]

                # remove the channel from the twitch clippers list
                for twitch_clipper in self.twitch_clippers[str(self.current_server_id)]:
                    if twitch_clipper.broadcaster_name == channel_name:
                        self.twitch_clippers[str(self.current_server_id)].remove(twitch_clipper)


                self.database.remove_channel(str(message.guild.id), channel_name)
                send_message_in_channel("Channel removed successfully", message.channel)
            except ValueError as e:
                send_message_in_channel(str(e), message.channel)

    # parameters: none
    def stop_command(self, message):
        if not self.is_enabled:
            send_message_in_channel("Bot is already stopped", message.channel)
            return

        send_message_in_channel("Bot is now stopped", message.channel)
        self.is_enabled = False

    # parameters: none
    def start_command(self, message):

        self.clip_channel_id = self.database.get_clip_channel_id(str(message.guild.id))

        if self.clip_channel_id is None:
            send_message_in_channel("No clip channel set, please set one by using /set_clip_channel channel_id", message.channel)
            return


        # if already enabled, error
        if self.is_enabled:
            send_message_in_channel("Bot has already started", message.channel)
            return

        send_message_in_channel("Bot has now started", message.channel)
        self.is_enabled = True
        self.clip_channel_id = self.database.get_clip_channel_id(str(message.guild.id))

    # parameters: channel_name
    def add_channel_command(self, message):

        user_message = message.content

        parameters = get_parameters(user_message)

        if parameters_check(1, parameters, message.channel):
            try:

                broadcaster_name = parameters[0]

                channel_names = self.database.get_channels(str(self.current_server_id))

                if broadcaster_name in channel_names:
                    send_message_in_channel("Channel is already added", message.channel)
                    return

                self.twitch_clippers[str(message.guild.id)].append(TwitchClipper(broadcaster_name))
                self.database.add_channel_to_server(str(message.guild.id), broadcaster_name)
                send_message_in_channel("Channel was added successfully", message.channel)

            except Exception as e:
                send_message_in_channel(str(e), message.channel)
                return

    # parameters: channel_id
    def set_clip_channel_command(self, message):
        user_message = message.content

        parameters = get_parameters(user_message)

        if parameters_check(1, parameters, message.channel):
            # get the channel id
            channel_id = parameters[0]
            try:
                clip_channel = self.get_channel(int(channel_id))
                if clip_channel is None:
                    send_message_in_channel("Error while setting channel", message.channel)
                else:
                    self.database.set_clip_channel_id(str(message.guild.id), int(channel_id))
                    send_message_in_channel("Channel was set successfully", message.channel)
            except Exception as e:
                print(str(e))
                send_message_in_channel("An invalid channel_id was given", message.channel)

    # parameters: client_id (twitch), broadcaster_id (twitch)
    def test_clip_command(self, message):
        user_message = message.content

        parameters = get_parameters(user_message)

        if parameters_check(1, parameters, message.channel):

            broadcaster_name = parameters[0]
            try:
                twitch_clipper = TwitchClipper(broadcaster_name)
            except Exception as e:
                send_message_in_channel(str(e), message.channel)
                return

            try:
                clip_id = twitch_clipper.clip()
            except Exception as e:
                send_message_in_channel(str(e), message.channel)
                return

            send_message_in_channel("Created clip: " + clip_id, message.channel)


class TwitchClipper:
    def __init__(self, broadcaster_name):
        self.BASE_URL = "https://api.twitch.tv/helix/clips"
        self.GET_TOKEN_BASE_URL = 'https://id.twitch.tv/oauth2/token'
        self.GET_USERS_BASE_URL = 'https://api.twitch.tv/helix/users'

        self.CLIENT_ID = "0de9dg1cqgt5hxjj3xm12lz3z6h8o4"
        self.CLIENT_SECRET = 'ott4ra2hy971mlo4tkfu9yxrzumxwa'
        self.GRANT_TYPE = 'client_credentials'
        self.OAUTH_TOKEN = "yj1s3lqexq60dtbawg7hdoc962vim3"
        self.broadcaster_name = broadcaster_name
        self.broadcaster_id = self.get_a_broadcaster_id(broadcaster_name)

    def get_oauth_token(self):
        body = {
            'client_id': self.CLIENT_ID,
            'client_secret': self.CLIENT_SECRET,
            "grant_type": self.GRANT_TYPE
        }

        r = requests.post("https://id.twitch.tv/oauth2/token", body)

        # data output
        keys = r.json()
        print(keys)

        return keys['access_token']

    def clip(self):
        # Set up the headers with client ID and OAuth token
        headers = {
            "Client-ID": self.CLIENT_ID,
            "Authorization": "Bearer " + self.OAUTH_TOKEN,
        }

        # Set up the request payload
        params = {
            "broadcaster_id": self.broadcaster_id,
            "has_delay": True
        }

        response = requests.post(self.BASE_URL, headers=headers, params=params)

        # Check the response status
        if response.status_code == 202:
            print("Clip creation request submitted successfully!")
            clip_data = response.json()
            clip_id = clip_data["data"][0]["id"]
            print(f"Clip ID: {clip_id}")
            return clip_id
        else:
            print("Failed to create the clip. Error code:", response.status_code)
            print(response.json())
            response_json = response.json()
            raise ValueError(f"Failed to create clip for the user {self.broadcaster_name}, error: {response_json['message']}")

    def get_a_broadcaster_id(self, name):
        # Set up the headers with client ID and OAuth token
        headers = {
            "Client-ID": self.CLIENT_ID,
            "Authorization": "Bearer " + self.OAUTH_TOKEN,
        }

        # Set up the request parameters
        params = {
            "login": name
        }

        # Make the GET request to retrieve the broadcaster ID
        response = requests.get(self.GET_USERS_BASE_URL, headers=headers, params=params)

        # Check the response status
        if response.status_code == 200:
            data = response.json()
            if data["data"]:
                broadcaster_id = data["data"][0]["id"]
                print(f"Broadcaster ID: {broadcaster_id}")
                return broadcaster_id
            else:
                print("Broadcaster not found.")
        else:
            print("Failed to retrieve broadcaster ID. Error code:", response.status_code)
            print(response.json())

        raise ValueError("Channel name is wrong")


class DataBase:
    def __init__(self):
        self.DATA_BASE_PATH_STR = "servers.json"

        self.data = {}

        # check if file exists before opening it
        if os.path.isfile(self.DATA_BASE_PATH_STR):
            with open(self.DATA_BASE_PATH_STR, 'r') as file:
                # Load the JSON data
                self.data = json.load(file)

    def on_server_join(self, server_id: str):

        if not self.server_exists(server_id):
            self.data[server_id] = {"clip_channel": None, "channels": []}

            self.update_database()

    def server_exists(self, server_id: str):
        return server_id in self.data

    def add_channel_to_server(self, server_id: str, channel_name):
        self.data[server_id]["channels"].append(channel_name)
        self.update_database()

    def get_clip_channel_id(self, server_id: str):
        print(type(server_id))
        for thing in self.data.keys():
            print(type(thing))
        return self.data[server_id]["clip_channel"]

    def set_clip_channel_id(self, server_id: str, clip_channel_id: int):
        self.data[server_id]["clip_channel"] = clip_channel_id
        self.update_database()

    def get_channels(self, server_id: str):
        return self.data[server_id]["channels"]

    def remove_channel(self, server_id: str, channel_name):
        if channel_name not in self.data[server_id]["channels"]:
            raise ValueError("Channel is not in the list")

        self.data[server_id]["channels"].remove(channel_name)
        self.update_database()

    def update_database(self):
        with open(self.DATA_BASE_PATH_STR, 'w+') as file:
            # Load the JSON data
            json.dump(self.data, file)


ff_clip_mat = FfClipMat()
ff_clip_mat.run(ff_clip_mat.TOKEN)
